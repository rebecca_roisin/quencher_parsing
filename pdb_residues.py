from Bio.PDB import *
import sys
from solvent_exposure import HSExposure_calc

# get file from pdb
#pdbl = PDBList()
#pdbl.retrieve_pdb_file('2AAK')

# parse pdb file -- that already exists in directory on computer
pdbfile = sys.argv[1]
parser = PDBParser()
structure = parser.get_structure('2AAK', pdbfile)

# get sequence
ppb=CaPPBuilder()
for pp in ppb.build_peptides(structure): 
    seq = pp.get_sequence()

# fluorescence quencing residues 
quenching = ["H", "W", "Y"]   

# make file of values
rf_csv =  open("/media/TOSHIBA EXT/repos/quencher_parsing/aa/pdb2aak_quenchers.csv", "w")
rf_csv.write("%s,%s,%s, %s,%s,%s\n" %("Residue 1", "Residue 2", "Distance", "Above", "Below", "Ratio"))

# get residues and sequence, dump them in file
model_count = 0.0
#hse = HSExposure()


for model in structure:
    model_count += 1
    count = 0.0
    ratios = HSExposure_calc(seq, model)
#    exp_cb = hse.calc_hs_exposure(model, option='CB')
#    for residue in model.get_residues():
#        print exp_cb[residue]
    for (residue1, aa1, ratio1) in zip(model.get_residues(), seq, ratios):
        if ratio1[0] >= 1:
            if aa1 in quenching:
                count += 1
                pos1 = residue1.get_id()[1]
                rf_csv.write("%s%s,%s%s,%s,%s,%s,%s\n" %(aa1, pos1, "--", "-", "-", ratio1[1], ratio1[2], ratio1[0]))
                for (residue2, aa2, ratio2) in zip(model.get_residues(), seq, ratios):
                    if ratio2[0] >= 1:
                        pos2 = residue2.get_id()[1]
                    #print "%s %s -- %s %s = " %(aa1, pos1, aa2, pos2), residue1["CA"] - residue2["CA"]
                        rf_csv.write("%s%s,%s%s,%s,%s,%s,%s\n" %(aa1, pos1, aa2, pos2, residue1["CA"] - residue2["CA"], ratio2[1], ratio2[2], ratio2[0]))
                rf_csv.write("\n")
    #print count
print "No. models = %s" %(model_count)

rf_csv.close()



        