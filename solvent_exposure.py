from Bio.PDB import *
import sys

# parse pdb file -- that already exists in directory on computer
pdbfile = sys.argv[1]
parser = PDBParser()
structure = parser.get_structure('2AAK', pdbfile)

# get sequence
ppb=CaPPBuilder()
for pp in ppb.build_peptides(structure): 
    seq = pp.get_sequence()

    # make file of values
rf_csv =  open("/media/TOSHIBA EXT/repos/quencher_parsing/aa/pdb2aak_solvent_exposure.csv", "w")
rf_csv.write("%s,%s\n" %("Residue 1", "Half-Sphere Exposure"))

model = structure[0]
hse = HSExposureCB(model, 13.0)

def HSExposure_calc(seq, model):
	exposed = []
	buried = []
	HS_ratios = []
	#exp_cb = hse.get_cb(model, option='CB')
	for i, j in zip(seq, hse):
		hseq = j[1]
		if hseq[0] != 0:
			ratio = float(hseq[1]) / hseq[0]
		else:
			ratio = hseq[1]
		if ratio >= 1:
			exposed.append((i,j))
		else:
			buried.append((i,j))
		HS_ratios.append((ratio, hseq[0], hseq[1]))
	return HS_ratios
HS_ratios = HSExposure_calc(seq, model)
print HS_ratios